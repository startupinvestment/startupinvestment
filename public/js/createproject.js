document.getElementById("createProjectForm").addEventListener("submit", async function(event) {
    event.preventDefault();
    
    const formData = new FormData(this);
    
    try {
        const response = await fetch('/create-project', {
            method: 'POST',
            body: formData,
            credentials: 'same-origin' // Ensures cookies are sent with the request if using sessions
        });

        if (!response.ok) {
            throw new Error('Failed to create project');
        }

        const data = await response.json();
        alert(data.message);
        // Optionally, redirect or perform other actions upon successful project creation
    } catch (error) {
        console.error(error);
        alert('Error creating project');
    }
});