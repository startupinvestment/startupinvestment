// async function fetchCampaignTransactions(campaignId) {
//     try {
//         const response = await fetch(`/projects/${campaignId}/transactions`);
//         const transactions = await response.json();
//         const transactionList = document.getElementById('transactionList');
//         transactionList.innerHTML = '';

//         if (transactions.length === 0) {
//             const noTransactionsMessage = document.createElement('p');
//             noTransactionsMessage.textContent = 'No transactions available.';
//             transactionList.appendChild(noTransactionsMessage);
//         } else {
//             // Reverse the order of transactions to display the latest ones at the top
//             transactions.reverse().forEach(transaction => {
//                 const transactionDiv = document.createElement('div');
//                 transactionDiv.innerHTML = `
//                     <p>Transaction ID: ${transaction.transactionId}</p>
//                     <p>Sender: ${transaction.sender}</p>
//                     <p>Receiver: ${transaction.receiver}</p>
//                     <p>Amount: ${transaction.amount}</p>
//                     <hr>
//                 `;
//                 transactionList.appendChild(transactionDiv);
//             });
//         }

//         openOverlay('transactionOverlay'); // Show the overlay
//     } catch (error) {
//         console.error('Error fetching campaign transactions:', error);
//     }
// }