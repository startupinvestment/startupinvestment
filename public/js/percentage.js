$(document).ready(function () {
    $('.kcontainer').each(function(index) {
        // Get raised and goal values for this project
        var raisedXRP = parseFloat($(this).find('.raised span').text()); // Convert to float
        var goalXRP = parseFloat($(this).find('.goal span').text()); // Convert to float

        // Check if the values are valid numbers
        if (!isNaN(raisedXRP) && !isNaN(goalXRP) && goalXRP !== 0) {
            // Calculate the percentage for this project
            var percentage = (raisedXRP / goalXRP) * 100;

            // Update the progress bar's value, display the percentage, and adjust the progress visually
            $(this).find('.progress').attr('value', percentage);
            $(this).find('.percentage-text').text(percentage.toFixed(2) + '%'); // Update percentage text

            // Adjust the progress visually for a smooth animation
            $(this).find('.progress').animate({ value: percentage }, {
                duration: 1000, // Animation duration in milliseconds
                step: function(now) {
                    $(this).attr('value', now);
                }
            });
        } else {
            // Handle cases where the values are not valid numbers or goalXRP is 0
            $(this).find('.percentage-text').text('N/A'); // Show "N/A" for percentage
        }
    });

})