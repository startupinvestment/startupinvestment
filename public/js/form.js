document.addEventListener('DOMContentLoaded', () => {
    const createProjectBtn = document.getElementById('createProjectBtn');
    const createProjectFormContainer = document.getElementById('createProjectFormContainer');
    const closeModalBtn = document.querySelectorAll('[data-bs-dismiss="modal"]');

    createProjectBtn.addEventListener('click', () => {
        createProjectFormContainer.style.display = 'block';
        createProjectFormContainer.classList.add('show');
        document.body.classList.add('modal-open');
    });

    closeModalBtn.forEach(btn => {
        btn.addEventListener('click', () => {
            createProjectFormContainer.style.display = 'none';
            createProjectFormContainer.classList.remove('show');
            document.body.classList.remove('modal-open');
        });
    });

    // Optional: Close the form when clicking outside or pressing ESC key
    window.addEventListener('click', (e) => {
        if (e.target === createProjectFormContainer) {
            createProjectFormContainer.style.display = 'none';
            createProjectFormContainer.classList.remove('show');
            document.body.classList.remove('modal-open');
        }
    });

    window.addEventListener('keydown', (e) => {
        if (e.key === 'Escape') {
            createProjectFormContainer.style.display = 'none';
            createProjectFormContainer.classList.remove('show');
            document.body.classList.remove('modal-open');
        }
    });

    // Handle form submission (optional)
    const createProjectForm = document.getElementById('createProjectForm');
    createProjectForm.addEventListener('submit', (e) => {
        e.preventDefault(); // Prevent default form submission
        // Your form submission logic here
        console.log('Form submitted!');
        // Close the modal if needed
        createProjectFormContainer.style.display = 'none';
        createProjectFormContainer.classList.remove('show');
        document.body.classList.remove('modal-open');
    });
});
