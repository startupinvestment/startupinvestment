// window.addEventListener('load', function() {
//     var heroContainer = document.querySelector('.hero-container');
//     heroContainer.classList.add('zoom-in');
// });

// window.addEventListener('scroll', function () {
//     var navbar = document.querySelector('.navbar');
//     if (window.scrollY > 0) {
//         navbar.classList.add('navbar-scrolled');
//     } else {
//         navbar.classList.remove('navbar-scrolled');
//     }
// });

document.addEventListener('DOMContentLoaded', function() {
    console.log('DOMContentLoaded event fired');
    
    var heroContainer = document.querySelector('.hero-container');
    heroContainer.classList.add('zoom-in');

    window.addEventListener('scroll', function() {
        // console.log('Scroll event fired');
        
        var navbar = document.querySelector('.navbar');
        if (window.scrollY > 0) {
            navbar.classList.add('navbar-scrolled');
        } else {
            navbar.classList.remove('navbar-scrolled');
        }
    });
});

