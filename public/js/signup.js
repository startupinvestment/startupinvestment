document.addEventListener('DOMContentLoaded', () => {
    const signUpForm = document.querySelector('#signUpModal form');
    const errorMessage = document.querySelector('#signUpModal .error-message');
    const successMessage = document.querySelector('#signUpModal .success-message');

    signUpForm.addEventListener('submit', async (event) => {
        event.preventDefault();

        const formData = new FormData(signUpForm);
        const userData = {
            name: formData.get('name'),
            email: formData.get('email'),
            userType: formData.get('userType'),
            walletAddress: formData.get('walletAddress'),
            walletSecret: formData.get('walletSecret'),
            password: formData.get('password'),
            confirmPassword: formData.get('confirm-password')
        };

        try {
            const response = await fetch('/register', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(userData)
            });

            if (response.ok) {
                const result = await response.text();
                showSuccess(result);
                // Optionally, redirect to another page after successful registration
                // window.location.href = '/success';
            } else {
                const errorText = await response.text();
                showError(errorText);
            }
        } catch (error) {
            console.error('Error:', error);
            showError('Registration failed. Please try again later.');
        }
    });

    function showError(message) {
        errorMessage.textContent = message;
        errorMessage.style.display = 'block';
        
    }

    function showSuccess(message) {
        successMessage.textContent = message;
        successMessage.style.display = 'block';
    }
});
