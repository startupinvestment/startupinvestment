document.addEventListener('DOMContentLoaded', () => {
    const confirmLogout = document.querySelector('.yes-btn');
    const alertModal = new bootstrap.Modal(document.getElementById('alertModal'));

    confirmLogout.addEventListener('click', async () => {
        try {
            const response = await fetch('/logout', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            if (response.ok) {
                showAlert('Logout successful', 'success');
            } else {
                showAlert('Logout failed', 'danger');
                console.error('Logout failed');
            }
        } catch (error) {
            showAlert('Error logging out', 'danger');
            console.error('Error logging out:', error);
        }
    });

    function showAlert(message, type) {
        const alertMessage = document.getElementById('alertMessage');
        const okButton = document.getElementById('alertOkButton');
        alertMessage.textContent = message;
        alertModal._dialog.classList.remove('modal-success', 'modal-danger');
        alertModal._dialog.classList.add(`modal-${type}`);
        alertModal.show();

        okButton.addEventListener('click', () => {
            alertModal.hide();
            if (type === 'success') {
                // Redirect the user to the landing page
                window.location.href = '/';
            }
        });
    }
});
