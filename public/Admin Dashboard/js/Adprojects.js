document.addEventListener('DOMContentLoaded', async () => {
  const projectsContainer = document.getElementById('projects');

  // Fetch projects from the API
  async function fetchProjects() {
    try {
      const response = await fetch('/projects/all');
      const projects = await response.json();
      console.log('Fetched projects:', projects); // Debugging
      displayProjects(projects);
    } catch (error) {
      console.error('Error fetching projects:', error);
    }
  }

  // Display projects in the DOM
  function displayProjects(projects) {
    console.log('Displaying projects:', projects); // Debugging
    if (projects.length === 0) {
      projectsContainer.innerHTML = '<p>No projects found.</p>';
      return;
    }

    const table = document.createElement('table');
    table.classList.add('table', 'table-striped');

    const thead = document.createElement('thead');
    thead.innerHTML = `
      <tr>
        <th>Image</th>
        <th>Title</th>
        <th>Status</th>
        <th>Target Amount</th>
        <th>Created At</th>
        <th>Actions</th>
      </tr>
    `;
    table.appendChild(thead);

    const tbody = document.createElement('tbody');

    projects.forEach(project => {
      console.log('Project:', project); // Debugging

      const row = document.createElement('tr');

      // Convert the buffer to a Base64 string safely
      let imageSrc = '';
      if (project.image && project.image.data) {
        const binary = new Uint8Array(project.image.data).reduce((data, byte) => data + String.fromCharCode(byte), '');
        imageSrc = `data:image/png;base64,${btoa(binary)}`;
      }

      row.innerHTML = `
        <td><img src="${imageSrc}" class="img-thumbnail" style="max-width: 100px;"></td>
        <td>${project.title}</td>
        <td>${project.status}</td>
        <td>${project.targetAmount} XRP</td>
        <td>${new Date(project.createdAt).toLocaleString()}</td>
        <td>
          <button class="approve-button" onclick="approveProject('${project.projectId}')">Approve</button>
          <button class="rejected-button" onclick="rejectProject('${project.projectId}')">Reject</button>
          <button class="view-button" onclick="viewProjectDetails('${project.projectId}')">View More</button>
          <button class="delete-button" onclick="deleteProject('${project.projectId}')">Delete</button>
        </td>
      `;
      tbody.appendChild(row);
    });

    table.appendChild(tbody);
    projectsContainer.innerHTML = ''; // Clear existing content
    projectsContainer.appendChild(table);
  }

  // Function to handle viewing project details
  window.viewProjectDetails = async (projectId) => {
    const project = await fetchProjectDetails(projectId);
    displayProjectDetails(project);
    $('#projectDetailsModal').modal('show');
  };

  // Fetch project details
  async function fetchProjectDetails(projectId) {
    try {
      const response = await fetch(`/projects/${projectId}`);
      const project = await response.json();
      return project;
    } catch (error) {
      console.error('Error fetching project details:', error);
    }
  }

  // Display project details in the modal
  function displayProjectDetails(project) {
    const projectDetailsContent = document.getElementById('projectDetailsContent');
    projectDetailsContent.innerHTML = `
      <p><strong>Title:</strong> ${project.title}</p>
      <p><strong>Description:</strong> ${project.description}</p>
      <p><strong>Target Amount:</strong> ${project.targetAmount} XRP</p>
      <p><strong>Status:</strong> ${project.status}</p>
      <p><strong>Created At:</strong> ${new Date(project.createdAt).toLocaleString()}</p>
      <p><strong>End Date:</strong> ${new Date(project.endDate).toLocaleString()}</p>
    `;
  }

  // Function to approve a project
  window.approveProject = async (projectId) => {
    try {
      const response = await fetch(`/projects/${projectId}/approve`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      if (response.ok) {
        showSuccessMessage('Project approved successfully');
        await fetchProjects(); // Refresh projects list
      } else {
        const errorData = await response.json();
        showErrorMessage(`Error approving project: ${errorData.error}`);
        console.error('Error approving project:', errorData);
      }
    } catch (error) {
      showErrorMessage('Error approving project');
      console.error('Error approving project:', error);
    }
  };

  // Function to delete a project
  window.deleteProject = async (projectId) => {
    if (confirm('Are you sure you want to delete this project?')) {
      try {
        const response = await fetch(`/projects/${projectId}`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json'
          }
        });
        if (response.ok) {
          showSuccessMessage('Project deleted successfully');
          await fetchProjects(); // Refresh projects list
        } else {
          const errorData = await response.json();
          showErrorMessage(`Error deleting project: ${errorData.error}`);
          console.error('Error deleting project:', errorData);
        }
      } catch (error) {
        showErrorMessage('Error deleting project');
        console.error('Error deleting project:', error);
      }
    }
  };

  // Function to reject a project
  window.rejectProject = async (projectId) => {
    try {
      const response = await fetch(`/projects/${projectId}/reject`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      if (response.ok) {
        showSuccessMessage('Project rejected successfully');
        await fetchProjects(); // Refresh projects list
      } else {
        const errorData = await response.json();
        showErrorMessage(`Error rejecting project: ${errorData.error}`);
        console.error('Error rejecting project:', errorData);
      }
    } catch (error) {
      showErrorMessage('Error rejecting project');
      console.error('Error rejecting project:', error);
    }
  };

  // Function to show success message
  function showSuccessMessage(message) {
    const successMessageContent = document.getElementById('successMessageContent');
    successMessageContent.textContent = message;
    $('#successModal').modal('show');
  }

  // Function to show error message
  function showErrorMessage(message) {
    const errorMessageContent = document.getElementById('errorMessageContent');
    errorMessageContent.textContent = message;
    $('#errorModal').modal('show');
  }

  // Initialize by fetching and displaying projects
  await fetchProjects();
});